import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title: String;
  descripcion: String = '';

  private mic_off: any = {
    icon: 'fa-play',
    color: 'warn',
    texto: 'Grabar'
  };
  private mic_on: any = {
    icon: 'fa-stop',
    color: 'primary',
    texto: 'Detener'
  };

  private mic: any = this.mic_off;
  private recognition: any;
  private recognizing: Boolean;

  @ViewChild('recognitionResult') recognitionResult: ElementRef;

  private initializeRecognition = () => {
    if (!this.recognition) {
      return;
    }

    this.recognition.lang = 'es-ES';
    this.recognition.continuous = true;
    this.recognition.interimResults = true;

    this.recognition.onstart = () => {
      console.log('[SpeechRecognition]', 'start ');
      this.recognizing = true;
      // this.recognitionResult.nativeElement.innerText = '';
    };

    this.recognition.onresult = (event) => {
      const results = Array.prototype.slice.call(event.results);
      const result = results.find((e) => {
        return !!e.isFinal || !!e[0].final;
      });

      if (result) {
        const resultText = result[0].transcript;
        console.log('[SpeechRecognition]', 'result', resultText);
        this.descripcion = this.descripcion + ' ' + resultText;
        this.recognitionResult.nativeElement.innerText = this.descripcion;
        // this.descripcion = this.descripcion + ' ' + resultText;
        // this.renderer.setValue(this.recognitionResult, resultText);
      }
    };

    this.recognition.onerror = (event) => {
      console.error(event);
    };

    this.recognition.onend = () => {
      console.log('[SpeechRecognition]', 'end');
      this.recognizing = false;
      this.mic = this.mic_off;
    };
  }

  constructor() {
    this.title = 'Reconocimiento de voz';
    this.recognition = null;
    this.recognizing = false;

    const _window = (<any>window);
    this.recognition = new _window.webkitSpeechRecognition();
    this.initializeRecognition();
  }


  processSpeech() {
    console.log('Iniciando el speech recognition');

    if (this.recognizing === false) {
      this.recognition.start();
      this.recognizing = true;
      this.mic = this.mic_on;
    } else {
      this.recognition.stop();
      this.recognizing = false;
      this.mic = this.mic_off;
    }
  }

}
