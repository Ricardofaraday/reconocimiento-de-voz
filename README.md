# Reconocimiento de Voz

Este proyecto muestra una aplicacion de reconocimiento de voz haciendo uso de la web speech api de google

### Como iniciar

$ git clone https://bitbucket.org/Ricardofaraday/reconocimiento-de-voz.git

$ cd reconocimiento-de-voz

$ npm install

$ npm start
